package club.mzywucai.blog;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
/**
 * @author mzywucai
 * @Description
 * @date 2019/7/19
 */

public class MyKnownWords {
    private Set<String> knownWords;

    public MyKnownWords() {
        knownWords = new HashSet<>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream("known/word.txt")));

            String tmpStr;
            while (((tmpStr = reader.readLine()) != null)) {
                tmpStr = tmpStr.trim();
                knownWords.add(tmpStr);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());

            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    public Set<String> getKnown() {
        return this.knownWords;
    }
}

package club.mzywucai.blog;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mzywucai
 * @Description
 * @date 2019/7/18
 */

public class SplitWord {
    /**
     * 字母组合
     */
    public static final String REGEX_CHAR = "^[A-Za-z]+$";


    /**
     * 大写字母组合
     */
    public static final String REGEX_CHAR_UC = "^[A-Z]+$";


    /**
     * 小写字母组合
     */
    public static final String REGEX_CHAR_LC = "^[a-z]+$";




    private static final int LAST = 32;
    private static final String PREFIX = "# UNIT ";


    private static Pattern matchLowLetter;
    private static Pattern matchHighLetter;
    private static Pattern matchLowAndHighLetter;
    private static MyKnownWords myKnownWords;

    private static Map<String, Integer> wordMap;
    private static List<Integer> rankList;
    private static Set<String> hotWords;

    static {
        matchLowLetter = Pattern.compile(REGEX_CHAR_LC);
        matchHighLetter = Pattern.compile(REGEX_CHAR_UC);
        matchLowAndHighLetter = Pattern.compile(REGEX_CHAR);
        myKnownWords = new MyKnownWords();

        wordMap = new TreeMap<>();
        rankList = new ArrayList<>();
        hotWords = new HashSet<>();
    }

    public static void main(String[] args) {


        for (int i = 0; i < LAST; i++) {
            InputStream input = getInput(i);

            getWordList(input);
        }
        getRankList();

        getHotWord(0, 1000, 6, 4);
        getHotWord(1000, 2000, 5, 4);
        removeKnownWord();
        writeWord();

        System.err.println("== END ==");
    }

    public static void writeWord() {
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("hot-word/hot.md")));
            int count = 50;
            int unit = 1;
            for (String word : hotWords) {
                if (count == 50) {
                    count = 1;

                    writer.write("\n");
                    writer.write(PREFIX + unit + "\n");
                    unit++;
                }

                writer.write(word + "\n");
                count++;
                writer.flush();
            }


        } catch (Exception e) {
            System.err.println(e.getMessage());
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    public static InputStream getInput(Integer name) {
        InputStream input = null;

        try {
            input = new FileInputStream("data/" + name + ".txt");

        } catch (Exception e) {
            System.err.println("input get error: " + e.getMessage());
        }

        return input;
    }

    public static void getWordList(InputStream input) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));

        String tmpStr;

        try {
            String[] splits;
            while (((tmpStr = reader.readLine()) != null)) {
                tmpStr = tmpStr.replaceAll("\\pP", " ");

                splits = tmpStr.split(" ");
                for (String tmp : splits) {
                    boolean matched = false;
                    Matcher matcher;
                    matcher = matchLowLetter.matcher(tmp);
                    if (matcher.matches())
                        matched = matcher.matches();
                    matcher = matchHighLetter.matcher(tmp);
                    if (matcher.matches())
                        matched = matcher.matches();
                    matcher = matchLowAndHighLetter.matcher(tmp);
                    if (matcher.matches())
                        matched = matcher.matches();
                    if (isContainsChinese(tmp))
                        matched = false;

                    tmp =  handleFilter(tmp);
                    if (null == tmp) {
                        continue;
                    }
                    if (matched) {
                        if (wordMap.containsKey(tmp)) {
                            wordMap.put(tmp, wordMap.get(tmp) + 1);
                        } else {
                            wordMap.put(tmp, 1);
                        }
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();

            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

    }


    public static String handleFilter(String word) {
        word = word.trim();

        if (word.length() <= 1)
            return null;

         word = word.toLowerCase();

        char firstLetter;
        firstLetter = word.charAt(0);
        int flag = 0;
        for (int i=1; i<word.length(); i++) {
            if (word.charAt(i) == firstLetter) {
                flag++;
            }
        }

        if (flag >= word.length() - 1)
            return null;

        return word;
    }

    public static void getRankList() {
         for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {
            rankList.add(entry.getValue());
         }

         Collections.sort(rankList);
         Collections.reverse(rankList);
    }

    public static boolean isContainsChinese(String str) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile(regEx);

        Matcher matcher = pat.matcher(str);
        boolean flag = false;
        if (matcher.find()) {
            flag = true;
        }
        return flag;
    }

    public static void getHotWord(int wordsBegin, int wordEnd, int wordLens, int wordTimes) {
        if (wordsBegin >= wordEnd) {
            return;
        }
        Integer high = rankList.get(wordsBegin);
        Integer low = rankList.get(wordEnd);

        for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {

            if (entry.getValue() >= low && entry.getValue() <= high &&
                    entry.getKey().length() >= wordLens && entry.getValue() >= wordTimes) {
                hotWords.add(entry.getKey());
            }
        }
    }

    public static void removeKnownWord() {
        Set<String> sub = new HashSet<>();
        sub.addAll(hotWords);
        sub.removeAll(myKnownWords.getKnown());

        hotWords = sub;
    }


}
